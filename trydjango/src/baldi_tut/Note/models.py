from django.db import models
from datetime import datetime

# Create your models here.

class Note(models.Model):
    title       = models.CharField(max_length=50)
    description = models.TextField()
    created_at  = models.DateTimeField(default=datetime)
    status      = models.IntegerField(default=0)