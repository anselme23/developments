const bcrypt = require('bcrypt')
async function hashPassword(password){
    const salt = await bcrypt.genSalt(10)
    const hashed = await bcrypt.hash(password, salt)
    return hashed
}
hashPassword('456')
module.exports = hashPassword