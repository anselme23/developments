const mongoose = require('mongoose')
var OrdersSchema = new mongoose.Schema({
    clientId:{
        type: String,
        required: true
    },
    request_place:{
        type: String,
        required: true
    },
    request_destination:{
        type : String,
        required: true
    },
    transporter_brand:{
        type: String,
        required: true
    },
    order_date:{
        type: Date,
        required: true
    },
    status:{
        type: Number,
        required: true
    },
    products_id:{
        type: Array,
        required: true
    },
    produt_quantities:{
        type: Array,
        required: true,
    }
})
mongoose.model('Order',OrdersSchema)