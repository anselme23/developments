const mongoose = require('mongoose')
const config = require('config')
const Joi = require('joi')
const jwt = require('jsonwebtoken')
var userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true,
        maxlength: 255,
        minlength: 3
    },
    email:{
        type: String,
        unique: true,
        required: true,
        maxlength: 255,
        minlength: 3
    },
    password:{
        type: String,
        required: true,
        maxlength: 1024,
        minlength: 3
    }
})
userSchema.methods.generateAuthToken=function(){
    const token = jwt.sign({_id:this._id,name:this.name,email:this.email},config.get('jwtPrivateKey'))
    return token
}
const User = mongoose.model('User',userSchema)
function validateUser(user){
    const schema = {
        name: Joi.string().max(255).min(3).required(),
        email: Joi.string().max(255).min(3).required().email(),
        password: Joi.string().max(255).min(3).required()
    }
    return Joi.validate(user,schema)
}
module.exports.User=User
module.exports.validate=validateUser