const mongoose = require('mongoose')
var SupplierSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    company_name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    telephone:{
        type:String,
        required:true
    },
    localtion:{
        type:String,
        required:true
    },
})
mongoose.model('Supplier',SupplierSchema)