const mongoose = require('mongoose')
mongoose.connect('mongodb://192.168.1.199/GrapesGMS',{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: true
})
.then(()=>console.log('connected to db successfully'))
.catch(err=>console.log('Failed to connect to mongodb',err))
require('./brand.model')
require('./category.model')
require('./product.model')
require('./orders.model')
