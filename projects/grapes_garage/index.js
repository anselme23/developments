require('./model/mongodb')
const config = require('config')
var multipart = require('connect-multiparty');


const BrandController = require("./controllers/brandController");
const CategoryController = require("./controllers/categoryController");
const ProductController = require("./controllers/productController");
const OrderController = require("./controllers/orderController");

const express = require('express')
const app = express()
const bodyparser = require('body-parser')

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.use(multipart());

app.use(bodyparser.urlencoded({extended:true}))

app.use(bodyparser.json())

app.use('/products_images', express.static(process.cwd() + '/public/products/'))

if(!config.get("jwtPrivateKey")){
    console.log('JWT PRIVATE KEY IS NOT DEFINED...')
    process.exit(1)
}
app.get('/',(req,res)=>{
    res.send('Welcome to product-category app')
})

app.use('/api/brands', BrandController)
app.use('/api/categories', CategoryController)
app.use('/api/products', ProductController)
app.use('/api/orders', OrderController)


const port = process.env.PORT || 9000
app.listen(port,()=>{console.log(`Server running on port ${port}...`)})
