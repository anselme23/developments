const express=require('express')
const mongoose = require('mongoose')
var router = express.Router()

const Brand = mongoose.model('Brand')
    router.post('/',(req,res)=>{
        var brand = new Brand()
        brand.name = req.body.name,
        brand.description = req.body.description
        brand.save()
          .then(brands=>res.send(brands).status(201))
          .catch(err=>res.send(err).status(400))
    })
    router.put('/',(req,res)=>{
        Brand.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
         .then(brands=>res.send(brands))
         .catch(err=>res.send(err).status(400))
    })
    router.get('/',(req,res)=>{
        Brand.find()
        .then(brands=>res.send(brands))
        .catch(err=>res.send(err).status(404))
    })
    router.get('/:id',(req,res)=>{
        Brand.find({_id:req.params.id})
        .then(brands=>res.send(brands))
        .catch(err=>res.send(err).status(404))
    })
    router.get('/byname/:name',(req,res)=>{
        Brand.find({name:req.params.name})
        .then(brands=>res.send(brands))
        .catch(err=>res.send(err).status(404))
    })
    router.delete('/:id',(req,res)=>{
        Brand.findOneAndRemove({_id:req.params.id})
        .then(brands=>res.send(brands))
        .catch(err=>res.send(err).status(404))
    })
module.exports=router