const express = require('express')
const mongoose = require('mongoose')
var fs = require('fs')
var router = express.Router()

const Product = mongoose.model('Product')
async function getRandString(num){
  let letters = "abcdefghijklmnopqrstuvwxyz1234567890";
  let str = '';
  let abc = letters.split("")

  for(i = 0; i < num ; i++){
    str += abc[Math.ceil((Math.random(0,str.length))*100)%36]
  }
  return str;
}
async function getData(req){
  var paths = []
  if(req.files.imagePaths.constructor === Array){
    // hello 
  }else{  
    let test = req.files.imagePaths;
    req.files.imagePaths = []
    req.files.imagePaths[0] = test  
  }
  var hey = getRandString(20);
  for(i = 0; i < req.files.imagePaths.length ; i++){
    var name = req.files.imagePaths[i].name.split('.')
    var ext = name[name.length -1 ];
    var imgname = await hey + i + new Date().getUTCMilliseconds().toString() +"."+ext
    let newName = __dirname+"/../public/products/"+imgname
    paths[i] = imgname
    fs.rename(req.files.imagePaths[i].path,newName, function (err) {
      if (err) throw err
    })
  }
  var product = {
    unique_id: req.body.unique_id,
    name: req.body.name,
    categoryId: req.body.categoryId,
    brandId: req.body.categoryId,
    price: req.body.price,
    description: req.body.description,
    items_in_stock: 0,
    imagePaths: paths
  }
  return product;
}
router.post('/',async (req, res) => {
  let p = await getData(req);
  let product = new Product(p);
  try {
    let newProduct = await product.save();
    if (newProduct) {
      return res.send(newProduct)
    } else {
      return res.send("Not registerred")
    }
  } catch (error) {
    return res.send(error)
  }
})

router.put('/add_amount/:id',async (req,res)=>{
  try {
    let newProduct = await Product.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true })
    res.send(newProduct)
  } catch (error) {
    res.send(error)
  }
})
router.put('/', async (req, res) => {
  let p = await getData(req);
  Product.findOneAndUpdate({ _id: req.body._id }, p , { new: true })
    .then(product => res.send(product))
    .catch(err => res.send(err).status(400))
})
router.get('/', (req, res) => {
  Product.find()
    .then(products => res.send(products))
    .catch(err => res.send(err).status(404))
})
router.get('/:id', (req, res) => {
  Product.find({ _id: req.params.id })
    .then(products => res.send(products))
    .catch(err => res.send(err).status(404))
})
router.get('/byName/:name', (req, res) => {
  Product.find({ name: req.params.name })
    .then(product => res.send(product))
    .catch(err => res.status(404).send(err))
})
router.delete('/:id', (req, res) => {
  Product.findByIdAndRemove({ _id: req.params.id })
    .then(product => res.send(product))
    .catch(err => res.status(404).send(err))
})
module.exports = router