<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Profile extends Model
    {


        protected $fillable = [
            'title',
        ];

        public function profileImage(){
            return ($this->image) ? $this->image : 'uploads/pf.png';
        }

        public function followers(){
            return $this->belongsToMany(User::class);
        }
        
        public function user()
        {
            return $this->belongsTo(User::class);
        }

    }
?>