<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;


class PostsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $users = auth()->user()->following()->pluck('profiles.user_id');
        $posts = Post::whereIn('user_id',$users)->latest()->get();
        
        return view('posts.index', [
            'posts' => $posts
        ]);
    }

    public function create()
    {
        return view("posts.create");
    }
    public function store()
    {
        // $data = request()->validate([
        //     'title' => ['required','max:200'],
        //     'description' => ['required'],
        //     'album' => ['required'],
        //     'image' =>['required']
        // ]);


        // // $imagePath = request('image')->store('uploads','public');

        // auth()->user()->posts()->create([
        //     'title' => $data['title'],
        //     'description' => $data['description'],
        //     'image' => $imagePath
        // ]);

        // return redirect('/profile/'.auth()->user()->id);
        dd(request());
    }

    public function show($post){
        $post = \App\Post::findorFail($post);
        return view('posts.show', [
            'post' => $post
        ]);
    }

}