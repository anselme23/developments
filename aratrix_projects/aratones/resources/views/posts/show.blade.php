@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <img src="/storage/{{ $post->image }}" alt="" style = 'width: 100%;'>
        </div>
        <div class="col-4">
            <div class = 'd-flex align-items-center'> 
                <div>
                    <img src="/storage/{{ $post->user->profile->profileImage() }}" class = 'rounded-circle mr-3' alt="" style = 'width: 30px'> 
                </div>
                <div>
                    <h5  class = 'font-weight-bold text-dark'><a href="/profile/{{ $post->user->id }}"><span class = 'text-dark'>{{ $post->user->name }}</span></a></h5>
                </div>
                <div>
                    <a href='#' class="ml-5 pb-5 ">Follow</a>
                </div>
            </div>
            <hr>
            <h2>{{ $post->title }}</h2>
            <p>
            <span  class = 'font-weight-bold text-dark'><a href="/profile/{{ $post->user->id }}"><span class = 'text-dark'>{{ $post->user->name }}</span></a></span>   &emsp;{{ $post->description }}
            </p>
            <p>
                Created at {{ $post->created_at }}
            </p>
        </div>
    </div>
</div>
@endsection
