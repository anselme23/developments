@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($posts as $post)
        <div class = 'row'>
            <div class="col-6 offset-3 post my-1 bg-white ">
                <div class="row py-2">
                    <div class="col-12">
                        <div class = 'd-flex align-items-baseline justify-content-between'> 
                            <div>
                                <img src="/storage/{{ $post->user->profile->profileImage() }}" class = 'rounded-circle mr-3' alt="" style = 'width: 30px'> 
                            </div>
                            <div>
                                <h5  class = 'font-weight-bold text-dark d-inline-block'><a href="/profile/{{ $post->user->id }}"><span class = 'text-dark'>{{ $post->user->name }} </span></a></h5>
                            </div>
                            <div>
                                Shared a new Post on {{ $post->created_at }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row py-2">
                    <div class="col-12">
                        <img src="/storage/{{ $post->image }}" alt="" style = 'width: 100%;'>
                    </div>
                </div>
                <div class = 'row py-2'>
                    <div class="col-12">
                        <h2>{{ $post->title }}</h2>
                        <p>
                            <span  class = 'font-weight-bold text-dark'><a href="/profile/{{ $post->user->id }}"><span class = 'text-dark'>{{ $post->user->name }}</span></a></span>   &emsp;{{ $post->description }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
