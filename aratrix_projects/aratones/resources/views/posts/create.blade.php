@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Add a New Post </h1>
    <form action="/posts" enctype = 'multipart/form-data' method = 'post'>

        @csrf

        <div class="row">
            <div class=" col-12 form-group row">
                <label for="title" class="col-md-4 col-form-label"> Post Title </label>

                <input id="title" name='title' type="text" class="form-control @error('title') is-invalid @enderror" title="title" value="{{ old('title') }}" autocomplete="title" autofocus>
            
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class=" col-12 form-group row">
                <label for="album" class="col-md-4 col-form-label"> Post album </label>

                <input id="album" name='album' type="text" class="form-control @error('album') is-invalid @enderror" title="album" value="{{ old('album') }}" autocomplete="album" autofocus>
            
                @error('album')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-12  form-group row">
                <label for="description" class="col-md-4 col-form-label"> Describe Your Post </label>

                <textarea id="description" class="form-control @error('description') is-invalid @enderror" title="description" name='description'></textarea>
            
                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-12  form-group row">
                <label for="image" class="col-md-4 col-form-label"> Add an Image </label>

                <input id="image" type="file" class="form-control-file @error('description') is-invalid @enderror" name='image'>
            
                @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-12  form-group row pt-5">
                <button class="btn btn-primary">Add New Post</button>
            </div>
        </div>
    </form>
</div>
@endsection
