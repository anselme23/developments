// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Router from 'vue-router'


import Home from './components/Home'
import Login from './components/Login'
import Register from './components/Register'
import Profile from './components/Profile'
import HeadSignUp from './components/HeadSignUp'
import ConfirmHSU from './components/ConfirmHSU'

Vue.use(Router)
const routes =  [
  { path: '/', name: 'Home', component: Home },
  { path: '/login', name: 'Login', component: Login },
  { path: '/register', name: 'Register', component: Register },
  { path: '/profile', name: 'Profile', component: Profile },
  { path: '/school-admin', name: 'HeadSignUp', component: HeadSignUp },
  { path: '/success/:randomCode', name: 'HeadSignUp', component: ConfirmHSU }
];

const router = new Router({
  routes,
  mode: 'history'
})

localStorage.setItem('router', router)

require('../node_modules/bootstrap/dist/css/bootstrap.css')

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  render: h => h(App),
  router,
}).$mount('#app')