<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/register','UserController@register');
Route::post('/login','UserController@login');
Route::get('/profile','UserController@getAuthenticatedUser');
Route::post('/positions/create','PositonsController@store');

Route::post('/positions/{position}/apply','PositonsController@apply');

Route::get('/all-users','UserController@all');
Route::get('/all-applies','HeadMasterController@getAll');
Route::post('/head-master/apply','HeadMasterController@apply');
Route::get('/head-master/{user}/accepted','HeadMasterController@accepted');

Route::get('/all-positions','PositonsController@getll');

Route::post('/success/{randomCode}','HeadMasterController@createSchool');
