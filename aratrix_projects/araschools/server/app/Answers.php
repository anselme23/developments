<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    protected $fillable =[
        'user_id','inputs_id','answer'
    ];

    public $timestamps = false;
    
}
