<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Positions;
use App\Inputs;
use App\Answers;
use App\Applies;


class PositonsController extends Controller
{
    public function __construct(){
        $user = parent::getTokenId();
        if(!$user){
            return "This Page is for authorized users";
        }else{
            return $user;
        }
    }

    public function store(Request $request)
    {

        $user = $this->__construct();
        
        if(isset($user->id) && $user->level == 1){

            $validator = Validator::make($request->json()->all() , [
                'title' => 'required|string', 
                'description' => 'required|string', 
                'level' => 'required|string', 
                'class' => 'required|string', 
                'input_no' => 'required', 
                'school_fees' => 'required',
                'input_types' => 'required|array',
                'input_placeholders' => 'required|array'
            ]);
            
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }

            if(sizeof($request['input_types']) != sizeof($request['input_placeholders']) && sizeof($request['input_placeholders']) != $request['input_no'] ){
                return request()->json([
                    "message" => "Make sure that It is working well"
                ]);
            }


            $position = Positions::create([
                'user_id' => $user->id, 
                'title' => $request->json()->get('title'), 
                'description' => $request->json()->get('description'), 
                'level' => $request->json()->get('level'), 
                'class' => $request->json()->get('class'), 
                'input_no' => $request->json()->get('input_no'), 
                'school_fees' => $request->json()->get('school_fees')
            ]);

            for($i = 0;$i < $request['input_no'];$i++){
                Inputs::create([
                    'positions_id' => $position->id,
                    'type' => $request['input_types'][$i],
                    'placeholder' => $request['input_placeholders'][$i],
                ]);
            }

            return "Sucessfully Submitted";
        }else{
            return response()->json(['message'=>'This is for the Authorized Users Only']);
        }
    }

    public function getll(){
        return Positions::all();
    }

    public function apply(Positions $position, Request $request){

        $user = $this->__construct();
        $validator = Validator::make($request->json()->all() , [
            'inputs_id' => 'required|array',
            'answer' => 'required|array'
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        for($i = 0;$i < $position['input_no'];$i++){
            Answers::create([
                'user_id' => $user['id'],
                'inputs_id' => $request['inputs_id'][$i],
                'answer' => $request['answer'][$i],
            ]);
        }

        Applies::create([
            'user_id' => $user['id'],
            'positions_id' => $position['id'],
        ]);

        return "Successfly Applied";
    }

}
