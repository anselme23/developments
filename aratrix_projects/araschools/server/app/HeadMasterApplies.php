<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeadMasterApplies extends Model
{
    protected $fillable = [
        'name','phone','email','country','status'
    ];
    protected $hidden = [
        'status','randomCode'
    ];
}
