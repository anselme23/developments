<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schools extends Model
{
    protected $fillable = [
        'user_id','name','description','email','password','country','province','district','sector'
    ];

    protected $hidden = [
        'user_id','password'
    ];
}
