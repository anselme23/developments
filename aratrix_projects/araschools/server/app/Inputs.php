<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inputs extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'positions_id','type','placeholder'
    ];
}
