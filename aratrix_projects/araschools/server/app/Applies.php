<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applies extends Model
{
    protected $fillable = [
        'user_id','positions_id'
    ];

    public $timestamps = false;
}
