<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotesController extends Controller
{
    public function create(Request $request){
        $user = parent::getTokenId();

        $validator = Validator::make($request->json()->all() , [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'body' => 'required|string',
        ]);

        if($validator->fails()){
            $errors = response()->json($validator->errors()->toJson(), 400);
            return $errors;
        }

        $user = Activity::create([
            'user_id' => $user->id,
            'title' => $request->json()->get('title'),
            'description' => $request->json()->get('description'),
            'body' => $request->json()->get('body'),
        ]);
        $message = "Successfully Created";
        return response()->json(compact('user','message'));
    }
}
