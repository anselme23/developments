<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Activity;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    public function create(Request $request)
    {
        $user = parent::getTokenId();

        $validator = Validator::make($request->json()->all() , [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'date' => 'required|date',
            'from' => 'required|date_format:H:i:s',
            'to' => 'required|date_format:H:i:s',
        ]);

        if($validator->fails()){
            $errors = response()->json($validator->errors()->toJson(), 400);
            return $errors;
        }
        $activity = Activity::create([
            'user_id' => $user->id,
            'title' => $request->json()->get('title'),
            'description' => $request->json()->get('description'),
            'date' => $request->json()->get('date'),
            'from' => $request->json()->get('from'),
            'to' => $request->json()->get('to'),
        ]);
        $message = "Successfully Created";
        return response()->json(compact('activity','message'));
    }
    public function showByDate($date){
        $activities = Activity::where('date','=',$date)->get();
        return response()->json(compact('activities'));
    }
}
